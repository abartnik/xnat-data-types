package net.bnac.cbi.xnat.plugin;

import org.nrg.xdat.bean.*;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.framework.annotations.XnatDataModel;
import org.springframework.context.annotation.Bean;

@XnatPlugin(value = "craniaDataTypes", name = "Crania Data Types",
        description = "XNAT data types for the CRANIA project",
        dataModels = {@XnatDataModel(value = CraniaThalamicvolumeBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Thalamic volume",
                                    plural = "Thalamic volumes",
                                    code = "TV"),
                      @XnatDataModel(value = CraniaSienaxBean.SCHEMA_ELEMENT_NAME,
                                    singular = "SIENAX volume",
                                    plural = "SIENAX volumes",
                                    code = "SV"),
                      @XnatDataModel(value = CraniaSienaBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Percent brain volume change",
                                    plural = "Percent brain volume changes",
                                    code = "PBVC"),
                      @XnatDataModel(value = CraniaAtrophiedlesionvolumeBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Atrophied lesion volume",
                                    plural = "Atrophied lesion volumes",
                                    code = "ALV"),
                    @XnatDataModel(value = CraniaLateralventricularvolumeBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Lateral ventricle volume",
                                    plural = "Lateral ventricle volumes",
                                    code = "LVV"),
                      @XnatDataModel(value = CraniaRsngraphtheorymeasurespearsonBean.SCHEMA_ELEMENT_NAME,
                                    singular = "RSN Graph Theory Measure",
                                    plural = "RSN Graph Theory Measures",
                                    code = "PGTM"),
                      @XnatDataModel(value = CraniaRsngraphtheorymeasuressparseBean.SCHEMA_ELEMENT_NAME,
                                    singular = "RSN Graph Theory Measure",
                                    plural = "RSN Graph Theory Measures",
                                    code = "SGTM"),
                      @XnatDataModel(value = CraniaMedullaoblongatavolumeBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Medulla Oblongata Volume",
                                    plural = "Medulla Oblongata Volumes",
                                    code = "MOV"),
                      @XnatDataModel(value = CraniaSalientcentrallesionvolumeBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Salient central lesion volume",
                                    plural = "Salient central lesion volumes",
                                    code = "SCLV"),
                      @XnatDataModel(value = CraniaNemonetworkefficiencyBean.SCHEMA_ELEMENT_NAME,
                                    singular = "NeMo Network Efficiency",
                                    plural = "NeMo Network Efficiencies",
                                    code = "NMNE"),
                      @XnatDataModel(value = CraniaThalamicstructuraldisconnectivityBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Thalamic structural disconnectivity",
                                    plural = "Thalamic structural disconnectivities",
                                    code = "TSD"),
                      @XnatDataModel(value = CraniaFirstBean.SCHEMA_ELEMENT_NAME,
                                    singular = "Subcortical volume",
                                    plural = "Subcortical volumes",
                                    code = "FIRST")
        })
public class CraniaDataTypes {
    @Bean
    public String craniaDataTypesMessage() {
        return "Is this thing on?";
    }
}
