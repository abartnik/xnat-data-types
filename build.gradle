/*
 * xnat-template-plugin: build.gradle
 * XNAT https://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

buildscript {
    ext {
        vXnat = "1.8.2"
    }
}

// TODO: This is the minimum set of Gradle plugins required to build most XNAT plugins.
//       You may include many others, including plugins for testing and test coverage,
//       IDE integration, and more.
plugins {
    id "jacoco"
    id "java"
    id "com.palantir.git-version" version "0.12.1"
    id "io.franzbecker.gradle-lombok" version "4.0.0"
    id "io.spring.dependency-management" version "1.0.11.RELEASE"
    id "org.nrg.xnat.build.xnat-data-builder" version "1.8.2"
}

// TODO: Change the group and version to values appropriate for your plugin project.
// NOTE: You should not leave the group value set to "org.nrg.xnatx.plugins"! Use
//       something to indicate who developed the plugin and follow the standard
//       Maven naming conventions: https://maven.apache.org/guides/mini/guide-naming-conventions.html
//       For example, if a group at Miskatonic University developed a plugin, the
//       group might be "edu.miskatonic.imaging.xnat".
group "net.bnac.cbi.xnat"
version "1.8.2"
description "XNAT data types for crania"

// TODO: This provides access to all of these repositories for dependency resolution.
repositories {
    mavenLocal()
    maven { url "https://nrgxnat.jfrog.io/nrgxnat/libs-release" }
    maven { url "https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot" }
    mavenCentral()
}

// TODO: This defines a dependency package, specifically the XNAT NRG parent pom, which specifies
//       versions for all of XNAT's dependencies. This helps ensure that plugins are building
//       against the same versions of various libraries as XNAT itself.
dependencyManagement.imports {
    mavenBom "org.nrg:parent:${project.version}"
}

// TODO: This is a pretty minimal set of dependencies, so don't worry if you need to add more.
dependencies {
    implementation enforcedPlatform("org.nrg:parent:${vXnat}")

    implementation("org.nrg.xnat:web") {
        transitive = false
    }
    implementation("org.nrg:dicom-xnat-mx") {
        transitive = false
    }
    implementation("org.nrg.xnat:xnat-data-models") {
        transitive = false
    }
    implementation "org.nrg.xdat:core"
    implementation "org.nrg:dicom-xnat-util"
    implementation "org.nrg:prefs"
    implementation "org.nrg:framework"
    implementation "dcm4che:dcm4che-core"
    implementation "io.springfox:springfox-swagger2"
    implementation "io.springfox:springfox-swagger-ui"

    compileOnly "log4j:log4j"

    testImplementation "org.junit.jupiter:junit-jupiter-api"
    testImplementation "org.springframework:spring-test"
//    testImplementation "com.github.sbrannen:spring-test-junit5"
    testImplementation "org.mockito:mockito-core"
    testImplementation "org.assertj:assertj-core"
    testImplementation "com.h2database:h2"

    testRuntimeOnly "org.junit.jupiter:junit-jupiter-engine"
    testRuntimeOnly "ch.qos.logback:logback-classic"
}

// TODO: This configures the compileJava task to call the xnatDataBuilder task before
//       trying to compile any Java code in the plugin. If your plugin includes one or
//       more custom data types, you'll need to generate source code for those datatypes
//       before you can use them in your Java code.
compileJava.dependsOn project.tasks["xnatDataBuilder"]

configurations {
    all {
        exclude group: "junit"
        exclude group: "net.logstash.logback"
        exclude group: "org.nrg.xnat.pipeline"
        exclude group: "org.slf4j", module: "jcl-over-slf4j"
        exclude group: "org.slf4j", module: "log4j-over-slf4j"
        exclude group: "org.slf4j", module: "slf4j-log4j12"
        exclude group: "org.slf4j", module: "slf4j-simple"
    }
    implementation.extendsFrom(implementAndInclude)
}

lombok {
    version = dependencyManagement.importedProperties["lombok.version"] as String
    sha256 = dependencyManagement.importedProperties["lombok.checksum"] as String
}

jacocoTestReport {
    dependsOn test
    reports {
        xml.enabled = false
        csv.enabled = true
        html.enabled = true
    }
}

java {
    // TODO: As of the 1.8.x release, XNAT is built as a Java 8-compatible (i.e. JDK 1.8)
    //       application. All plugins must be 1.8 compatible as well. This does NOT mean
    //       the code must be compiled with Java 8, just that the compiled byte code must
    //       be compatible with running in a Java 8 environment.
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

// TODO: This tells the compiler where to find source code. This isn't required in a
//       standard build, but the XNAT data builder generates code from XNAT data-type
//       schemas that the compiler needs to know about.
sourceSets {
    main {
        java {
            srcDir "build/xnat-generated/src/main/java"
        }
        resources {
            srcDir "build/xnat-generated/src/main/resources"
            // exclude {
                // TODO: Schema files get copied over to the generated source folder, which
                //       results in duplicate entries for those files in the plugin jar file.
                //       This exclusion filters those duplicates out.
                // "build/xnat-generated/src/main/resources/schemas/**/*.xsd"
            // }
        }
    }
}

// Pulls in the Jenkins BUILD_NUMBER environment variable if available.
def buildDate = new Date()
def buildNumber = System.getenv().BUILD_NUMBER?.toInteger() ?: "Manual"
def isDirty, branchName, gitHash, gitHashFull, commitDistance, lastTag, isCleanTag

try {
    def gitDetails = versionDetails()
    isDirty = gitVersion().endsWith ".dirty"
    branchName = gitDetails.branchName ?: "Unknown"
    gitHash = gitDetails.gitHash
    gitHashFull = gitDetails.gitHashFull
    commitDistance = gitDetails.commitDistance
    lastTag = gitDetails.lastTag
    isCleanTag = gitDetails.isCleanTag
} catch (IllegalArgumentException e) {
    logger.info "Got an error trying to read VCS metadata from git. It's possible this project is not under VCS control. Using placeholder values for manifest entries."
    isDirty = true
    branchName = "Unknown"
    gitHash = "None"
    gitHashFull = "None"
    commitDistance = 0
    lastTag = "None"
    isCleanTag = false
}

ext.gitManifest = manifest {
    attributes "Application-Name": project.description,
            "Build-Date": buildDate,
            "Build-Number": buildNumber,
            "Implementation-Version": project.version,
            "Implementation-Sha": gitHash,
            "Implementation-Sha-Full": gitHashFull,
            "Implementation-Commit": commitDistance,
            "Implementation-LastTag": lastTag,
            "Implementation-Branch": branchName,
            "Implementation-CleanTag": isCleanTag,
            "Implementation-Dirty": isDirty
}

logger.info """
Building artifacts with manifest attributes:

 * Build-Date:              ${buildDate}
 * Build-Number:            ${buildNumber}
 * Implementation-Version:  ${version}
 * Implementation-Sha-Full: ${gitHashFull}
 * Implementation-Sha:      ${gitHash}
 * Implementation-Commit:   ${commitDistance}
 * Implementation-LastTag:  ${lastTag}
 * Implementation-Branch:   ${branchName}
 * Implementation-CleanTag: ${isCleanTag}
 * Implementation-Dirty:    ${isDirty}
"""

task sourceJar(type: Jar, dependsOn: classes) {
    classifier "sources"
    manifest {
        from gitManifest
    }
    from sourceSets.main.allSource
}

task javadocJar(type: Jar) {
    classifier "javadoc"
    manifest {
        from gitManifest
    }
    from javadoc.destinationDir
}

jar {
    dependsOn test, sourceJar, javadocJar
    enabled = true
    manifest {
        from gitManifest
    }
}

task xnatPluginJar(type: Jar) {
    dependsOn test, sourceJar, javadocJar
    zip64 true
    archiveClassifier.set "xpl"
    manifest {
        from gitManifest
    }
    // files and folders with "-dev" or "--xx" in their name
    // will not be in the compiled jar
    exclude "**/resources/**/*-dev**"
    exclude "**/resources/**/*--xx**"
    from {
        configurations.implementAndInclude.collect { it.isDirectory() ? it : zipTree(it) }
    } {
        exclude "META-INF/*.SF"
        exclude "META-INF/*.DSA"
        exclude "META-INF/*.RSA"
    }
    with jar
}
