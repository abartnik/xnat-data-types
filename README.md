# CRANIA Data Types #

**Custom XNAT data types for the CRANIA project**</br>
This XNAT plugin defines data types to be used as the output for analyses that will eventually be queried by the 
CRANIA front end. This is done by editing the XML in   `./src/main/resources/schemas/crania/crania.xsd` to expand 
existing XNAT data types. The data types defined here should match the outputs defined in the 
`MR image data set analysis` subclasses on MRIO to allow integration of the XNAT PostgreSQL database with the ontology. 

# Building #

To build the XNAT plugin:

1. If you haven't already, clone this repository and cd to the newly cloned folder.
1. Build the plugin: `./gradlew jar` (on Windows, you can use the batch file: `gradlew.bat jar`). This should build the plugin in the file **build/libs/xnat-template-plugin-1.0.0.jar** (the version may differ based on updates to the code).
1. Copy the plugin jar to your plugins folder: `cp build/libs/crania-data-types-1.8.2.jar /data/xnat/home/plugins`
